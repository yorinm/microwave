﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace HW2
{
	class Food
	{

		public Food(BitmapImage link)
		{
			_foodlink = link;
		}
		private BitmapImage _foodlink;

		public BitmapImage Foodlink
		{
			get => _foodlink;
			set => _foodlink = value;
		}
	}

	
}
