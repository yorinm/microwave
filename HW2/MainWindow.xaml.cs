﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

// magnetron mag alleen gestart worden als de deur dicht is en er een tijd in de timer staat.
// eten kan alleen worden toegevoegd als de deur open is.


namespace HW2
{
	public partial class MainWindow
	{
		private BowlingApp.MainWindow newForm;
		private Boolean _powerState = false; // boolean voor state van magnetron
		private System.Timers.Timer _delayTimer; // hoofd timer
		private System.Timers.Timer _timer1; // countdown timer
		//variabele nodig voor timers
		private int _minutes = 0;
		private int _seconds = 0;
		private static int _counterstart = 0;
		private int _counter = _counterstart;
		private Boolean _isDoorClosed = true; // true == closed
		private  Boolean _timerstarted = false;// boolean om te checken of de timer loopt


		// food plaatjes
		private readonly Food _pasta = new Food(new BitmapImage(new Uri(@"pasta.png", UriKind.Relative)));
		private readonly Food _brood = new Food(new BitmapImage(new Uri(@"brood.png", UriKind.Relative)));
		private readonly Food _empty = new Food(new BitmapImage(new Uri(@"", UriKind.Relative)));

		public MainWindow()
		{
			InitializeComponent(); // gui
			//deur gesloten
			DoorClosed.Visibility = Visibility.Visible;
			DoorOpen.Visibility = Visibility.Hidden;
			_isDoorClosed = true;
		}

		//main timer is aantal minuten + seconden *1000 voor miliseconden + 1 seconden voor vreemde timer stop.
		private void Delay()
		{
			_delayTimer = new System.Timers.Timer();
			_delayTimer.Interval = _counterstart * 1000 + 1000;
			//_delayTimer.Enabled = true;
			_delayTimer.Elapsed += _delayTimer_Elapsed;
			_delayTimer.Start();
			_timerstarted = true;
		}
	

		//event als timer afloopt
	private void _delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
	{
		StopMicrowave();
	}

	//start knop start de magnetron als de timer niet 0 is, de deur gesloten is en de timer niet loopt
	private void Start_Button(object sender, RoutedEventArgs e)
	{
		if(_counter !=0 && _isDoorClosed == true && _timerstarted == false)
		{
			StartMicrowave();// zet de lamp aan
			StartCountdown();// start de countdown
			_powerState = true;
		}
	}

	private void StartMicrowave()
	{
		Dispatcher.Invoke(() => // nodig voor timer threading
			{
				Delay();
					_powerState = true;
					Lampswitch(_powerState);
			});
	
	}

	private void StopMicrowave()
	{
		Dispatcher.Invoke(() => {
			// Code causing the exception or requires UI thread access
			_powerState = false;
			Lampswitch(_powerState);
			_delayTimer.Stop();
			_timer1.Stop();
			_timerstarted = false;
			//reset de timer text naar 00
			Minutes.Text = "00";
			Seconds.Text = "00";

		});
	}

	//lamp kleur switcher
	private void Lampswitch(Boolean powerstate)
	{
		switch (powerstate)
		{
			case true:
				Door.Fill = new SolidColorBrush(Color.FromArgb(125, 220, 220, 0));
				break;
			case false:
				Door.Fill = new SolidColorBrush(Color.FromArgb(125, 123, 123, 123));
				break;
		}
	}


	//1 seconden timer voor countdown
	private void StartCountdown()
	{
		_timer1 = new System.Timers.Timer();
		_timer1.Interval = 1000;
		_timer1.Elapsed += Countdown;
		_timer1.Start();
	}

	//code om de timer aan te sturen. stopt de magnetron als de timer afloopt.
	private void Countdown(object sender, System.Timers.ElapsedEventArgs e)
	{
		Dispatcher.Invoke(() =>
		{
			_counter--;
			Timer.Content = EditTimer(_counter);
			_counterstart = _counter;
			//Console.WriteLine("counter: " + counter);
			//Console.WriteLine("counterstart: " + counterstart);
			if (_counter == 0)
			{
				StopMicrowave();
			}
		});
		
	}

	//zet de timer op het scherm met een time formater
	private string EditTimer(int number)
	{
		_counterstart = number;
		_counter = number;
		TimeSpan time = TimeSpan.FromSeconds(number);


		return time.ToString(@"mm\:ss");
		}

	//voegt de ingevulde minuten toe aan de timer.
	private void AddMinutes(object sender, TextChangedEventArgs e)
	{
			_minutes = 0;
			if (Minutes.Text == "")
			{
				_minutes = 0;
			}
			else
			{
				_minutes += Convert.ToInt32(Minutes.Text) * 60;
			}
			Timer.Content = EditTimer(_minutes + _seconds);
		}

		//voegt de ingevulde secondes toe aan de timer.
		private void AddSeconds(object sender, TextChangedEventArgs e)
		{

			_seconds = 0;
			if (Seconds.Text == "")
			{
				_seconds = 0;
			}
			else
			{
				_seconds += Convert.ToInt32(Seconds.Text);
			}
			Timer.Content = EditTimer(_minutes + _seconds);

		}

		//Regex voor de timer invoer velden
		private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
		{
			Regex regex = new Regex("[^0-9]+");
			e.Handled = regex.IsMatch(e.Text);
		}

		//deur open code
		private void OpenDoor_MouseDown(object sender, RoutedEventArgs e)
		{
			if (_isDoorClosed == true)
			{
				DoorClosed.Visibility = Visibility.Hidden;
				DoorOpen.Visibility = Visibility.Visible;
				_isDoorClosed = false;
			}
			else
			{
				DoorClosed.Visibility = Visibility.Visible;
				DoorOpen.Visibility = Visibility.Hidden;
				_isDoorClosed = true;
			}


			if (_timerstarted == true)
			{
				if (_isDoorClosed == true)
				{
					_timer1.Start();
					_delayTimer.Start();
					_powerState = true;
				}
				else
				{
					_timer1.Stop();
					_delayTimer.Stop();
					_powerState = false;
				}
			}
			Lampswitch(_powerState);

		}

		//code voor het updaten van eten in de magnetron.
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (_isDoorClosed != true)
			{
				switch (Food.SelectionBoxItem)
				{
					case "Pasta":
						FoodImage.Source = _pasta.Foodlink;
						break;
					case "Brood":
						FoodImage.Source = _brood.Foodlink;
						break;
					case "":
						FoodImage.Source = _empty.Foodlink;
						break;
				}
			}
		}

		private void OpenBowling_Click(object sender, RoutedEventArgs e)
		{
			newForm = new BowlingApp.MainWindow();
			newForm.Show();
		}
	}
}
