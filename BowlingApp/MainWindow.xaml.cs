﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BowlingApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	
		
	public partial class MainWindow : Window
	{
		private int SelectedBal = 0;
		private Rectangle[] pion = new Rectangle[11];
		private int gooi = 1;
		private int beurt = 0;
		private int rest;
		private int geraakt;
		private Score _punten = new Score();
		public Label[,] label = new Label[10,4];
		private int score1, score2, totalscore, strikescore, sparescore;
		private Boolean strike, spare;
		private Random rnd = new Random();



		public MainWindow()
		{
			InitializeComponent();

			pion[1] = p1;
			pion[2] = p2;
			pion[3] = p3;
			pion[4] = p4;
			pion[5] = p5;
			pion[6] = p6;
			pion[7] = p7;
			pion[8] = p8;
			pion[9] = p9;
			pion[10] = p10;

			Volgende.IsEnabled = false;


			label[0, 0] = b1b1;
			label[0, 1] = b1b2;
			label[0, 3] = t1;

			label[1, 0] = b2b1;
			label[1, 1] = b2b2;
			label[1, 3] = t2;

			label[2, 0] = b3b1;
			label[2, 1] = b3b2;
			label[2, 3] = t3;

			label[3, 0] = b4b1;
			label[3, 1] = b4b2;
			label[3, 3] = t4;

			label[4, 0] = b5b1;
			label[4, 1] = b5b2;
			label[4, 3] = t5;

			label[5, 0] = b6b1;
			label[5, 1] = b6b2;
			label[5, 3] = t6;

			label[6, 0] = b7b1;
			label[6, 1] = b7b2;
			label[6, 3] = t7;

			label[7, 0] = b8b1;
			label[7, 1] = b8b2;
			label[7, 3] = t8;

			label[8, 0] = b9b1;
			label[8, 1] = b9b2;
			label[8, 3] = t9;

			label[9, 0] = b10b1;
			label[9, 1] = b10b2;
			label[9, 3] = t10;




		}


		private void OranjeBal_MouseDown(object sender, MouseButtonEventArgs e)
		{
			SelectedBal = 1;
			OranjeBal.StrokeThickness = 10;
			RodeBal.StrokeThickness = 1;
		}

		private void RodeBal_MouseDown(object sender, MouseButtonEventArgs e)
		{
			SelectedBal = 2;
			OranjeBal.StrokeThickness = 1;
			RodeBal.StrokeThickness = 10;
		}

		

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			
			if (gooi == 1)
			{
				//eerste gooi
				rest = gooi1();
				ScoreDrawer();
				gooi = 2;
			}
			else if(gooi == 2)
			{
				//tweede gooi
				if (rest != 0)
				{
					gooi2(rest);
					ScoreDrawer();
					if (beurt >= 9)
					{
						Volgende.IsEnabled = false;
					}
					else
					{
						Volgende.IsEnabled = true;
					}

					Gooien.IsEnabled = false;
				}

			}else
			{
				//reset alles
				if (beurt >= 9)
				{
					Volgende.IsEnabled = false;
				}
				else
				{
					Volgende.IsEnabled = true;
				}
				
				Gooien.IsEnabled = false;

			}
			
		}

		public int gooi1()
		{
			int random = rnd.Next(11);
			Aantal_geraakte.Content = random; // text in gui\
			_punten.addScore(beurt,gooi-1,random);
			geraakt = random;
			if (random == 10)
			{
				//strike
				strike = true;
				for (int i = 0; i <= random; i++)
				{
					if (i != 0)
					{
						pion[i].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
					}
				}
				ScoreWord.Content = "Strike";
				
					Volgende.IsEnabled = true;

					Gooien.IsEnabled = false;
				if (beurt == 10)
				{
					gooi = 3;
				}
				return 0;
			}
			if (random != 0)
			{
				for (int i = 0; i <= random; i++)
				{
					if (i != 0)
					{
						pion[i].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
					}
				}
				
			}
			return 10 - random;

		}


		public void gooi2(int rest)
		{
			int random = rnd.Next(0,rest +1);
			Aantal_geraakte.Content = random;
			_punten.addScore(beurt, gooi - 1, random);
			geraakt = geraakt + random;
			if ((10 - rest) != 0 || rest == 10)
			{
				for (int i = (geraakt - random); i <= (geraakt); i++)
				{
					if (i != 0)
					{
						pion[i].Fill = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
					}
				}
			}
			if(geraakt >= 10)
			{
				ScoreWord.Content = "Spare";
				spare = true;
			}
		}

		private void Volgende_Click(object sender, RoutedEventArgs e)
		{
			checkStrike();
			checkSpare();


			for (int i = 1; i <= 10; i++)
			{
				pion[i].Fill = new SolidColorBrush(Color.FromArgb(255,0,0,0));
			}

			gooi = 1;
			geraakt = 0;
			beurt++;
			Volgende.IsEnabled = false;
			Gooien.IsEnabled = true;
			ScoreWord.Content = "";
			score1 = 0;
			score2 = 0;
			strikescore = 0;
			sparescore = 0;


			int rowLength = _punten.score.GetLength(0);
			int colLength = _punten.score.GetLength(1);

			for (int i = 0; i < rowLength; i++)
			{
				for (int j = 0; j < colLength; j++)
				{
					Console.Write(string.Format("{0} ", _punten.score[i, j]));
				}
				Console.Write(Environment.NewLine + Environment.NewLine);
			}
		}

		private void ScoreDrawer()
		{
			if(beurt != 0) {
				_punten.score[beurt - 1, 2] += strikescore;
				_punten.score[beurt - 1, 2] += sparescore;
			}

			if (gooi == 1)
			{
				score1 = _punten.score[beurt, gooi - 1];
				totalscore += score1;
			}else if(gooi == 2)
			{
				score2 = _punten.score[beurt, gooi - 1];
				totalscore += score2;
			}

			label[beurt, gooi -1].Content = _punten.score[beurt, gooi-1];
			label[beurt, 3].Content = totalscore;
		}

		private void checkStrike()
		{
			if (beurt != 0 && strike)
			{
				_punten.score[beurt - 1, 2] += _punten.score[beurt, 2];
			}
		}

		private void checkSpare()
		{
			if (beurt != 0 && spare)
			{
				_punten.score[beurt - 1, 2] += _punten.score[beurt, 0];
			}
		}
	}
}
